const items=[
    {
        "id":1,
        "name":"Chocolate Truffle Cake (1kg)",
        "cost":85,
    },
    {
        "id":2,
        "name":"Black forest (1kg)",
        "cost":700,
    },
    {
        "id":3,
        "name":"Chocolate Lava Cake (1kg)",
        "cost":650,
    },
    {
        "id":4,
        "name":"Chocolate Oreo Cake (1kg)",
        "cost":700,
    },
    {
        "id":5,
        "name":"German Chocolate Cake (1kg)",
        "cost":900,
    },
    {
        "id":6,
        "name":"Pineapple Delight (1kg)",
        "cost":500,
    },
    {
        "id":7,
        "name":"Black Current (1kg)",
        "cost":550,
    },
    {
        "id":8,
        "name":"Blueberry (1kg)",
        "cost":400,
    },
    {
        "id":9,
        "name":"Mango Delight (1kg)",
        "cost":550,
    },
    {
        "id":10,
        "name":"Sweet Candy (1kg)",
        "cost":580,
    },
    {
        "id":11,
        "name":"Red velvet (1kg)",
        "cost":570,
    },
    {
        "id":12,
        "name":"Butterscotch (1kg)",
        "cost":400,
    },
    {
        "id":13,
        "name":"Chocolate Coconut Cake (1kg)",
        "cost":1000,
    },
    {
        "id":14,
        "name":"Baked Alaska (1kg)",
        "cost":1200,
    },
    {
        "id":15,
        "name":"Icebox Cake (1kg)",
        "cost":1050,
    },
    {
        "id":16,
        "name":"Royal Rasmalai (1kg)",
        "cost":900,
    },
    {
        "id":17,
        "name":"Rose Gulkand (1kg)",
        "cost":1150,
    }
];
var totalamount=0;
var itemcount=0;
function addTocart(id)
{
    var itemid = id;
    var item=items.find(item => item.id ===itemid);
    console.log(item);
    var itemcost= item.cost;
    var itemname= item.name;
    var quantity=1;
    itemcount+=1;
    totalamount+=quantity*itemcost;

    var cart= document.getElementById("carttabel");
    cart.innerHTML+= 
    `
    <tr>
      <th>${itemcount}</th>
      <th>${itemname}</th>
      <th>${quantity}</th>
      <th>${itemcost}</th>
      <th>${quantity*itemcost}</th>
    </tr>
    `
    var carttotal=document.getElementById("cartTotal");
    carttotal.innerHTML+=
    `
    <tr>
      <th></th>
      <th></th>
      <th></th>
      <th><b> Total = <?b></th>
      <th>${totalamount}</th>
    </tr>
    `
}